import axios from 'axios';

function createArticle(article) {
    return axios.post('/api/articles', article).then(response => response.data);
}

function retrieveArticles() {
    return axios.get('/api/articles').then(response => response.data);
}

function retrieveArticle(id) {
    return axios.get(`/api/articles/${id}`).then(response => response.data);
}

function updateArticle(article) {
    return axios.put(`/api/articles/${article.id}`, article).then(response => response.status);
}

function deleteArticle(id) {
    return axios.delete(`/api/articles/${id}`).then(response => response.status);
}

export {
    createArticle,
    retrieveArticles,
    retrieveArticle,
    updateArticle,
    deleteArticle
};