/**
 * This is a simple RESTful API for dealing with articles.
 */

import express from 'express';
import {
    createArticle,
    retrieveArticle,
    retrieveArticleList,
    updateArticle,
    deleteArticle
} from '../../articles-dao';

const router = express.Router();

// Create new article
router.post('/', (req, res) => {
    const { title, image, content } = req.body;
    const newArticle = createArticle({ title, image, content });

    res.status(201)
        .header('Location', `/api/articles/${newArticle.id}`)
        .json(newArticle);
})

// Retrieve all articles
router.get('/', (req, res) => {

    res.json(retrieveArticleList());
});

// Retrieve single article
router.get('/:id', (req, res) => {
    const { id } = req.params;

    const article = retrieveArticle(id);

    if (article) {
        res.json(article);
    }
    else {
        res.status(404).end();
    }
});

// Update article
router.put('/:id', (req, res) => {
    const { id } = req.params;
    const article = req.body;

    article.id = id;

    const success = updateArticle(article);

    if (success) {
        res.status(204).end();
    }
    else {
        res.status(404).end();
    }
});

// Delete article
router.delete('/:id', (req, res) => {
    const { id } = req.params;
    deleteArticle(id);
    res.status(204).end();
});

export default router;