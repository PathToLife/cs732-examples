window.addEventListener("load", () => {

    const btnGreeting = document.querySelector("#btn-greeting");
    const spanGreeting = document.querySelector("#greeting");
    const btnAdd = document.querySelector("#btn-add");
    const txtA = document.querySelector("#inp-a");
    const txtB = document.querySelector("#inp-b");
    const spanResult = document.querySelector("#result");

    btnGreeting.addEventListener("click", () => {

        axios.get("/api/greeting")
            .then(response => spanGreeting.innerHTML = response.data.message);

    });

    btnAdd.addEventListener("click", () => {

        axios.post("/api/calculate", {
            a: parseInt(txtA.value),
            b: parseInt(txtB.value)
        })
            .then(response => spanResult.innerHTML = response.data.result);

    });

});